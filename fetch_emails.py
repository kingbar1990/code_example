import imaplib
from email import message_from_bytes, header
import poplib
import chardet
import logging
import datetime
import pytz
from bs4 import BeautifulSoup
from email.utils import parseaddr, parsedate_to_datetime

from celery import task
from django.conf import settings
from case.models import BaseCase

from interactions.models import EmailInteraction

from .models import EmailBox
from .utils import special_folders, get_enduser, get_case_from_subject

logger = logging.getLogger('apps')


@task()
def fetch_mailbox():
    """Periodic task that fetches mail from mailbox"""
    for emailbox in EmailBox.objects.all().values('id', 'use_imap'):
        if emailbox.get('use_imap'):
            read_via_imap.delay(emailbox.get('id'))
        else:
            read_via_pop3.delay(emailbox.get('id'))


def imap_prepare_login(mailbox_id):
    mailbox = EmailBox.objects.get(id=mailbox_id)
    imaplib.Commands['XLIST'] = ('AUTH', 'SELECTED')
    imaplib._MAXLINE = mailbox.imap_setting.max_line
    if mailbox.imap_setting.use_tls:
        imap = imaplib.IMAP4_SSL(
            mailbox.imap_setting.server,
            mailbox.imap_setting.port
        )
    else:
        imap = imaplib.IMAP4(
            mailbox.imap_setting.server,
            mailbox.imap_setting.port
        )
    try:
        imap.login(mailbox.username, mailbox.get_password())
    except Exception as e:
        print("Login failed:", e)
    return imap, mailbox


def _create_msg_interaction(msg_num, imap, mailbox, folder, exists_msg_ids=()):
    _, response = imap.fetch(msg_num, '(RFC822)')
    message = message_from_bytes(response[0][1])
    msg_text = decode_message_txt(message)
    headers = decode_headers(message.items())
    message_id = headers.get('Message-ID').encode()
    if message_id not in exists_msg_ids:
        EmailInteraction.objects.create(
            folder=folder,
            case=get_case_from_subject(headers.get('Subject')),
            email_box_id=mailbox.id,
            email_id=message_id,
            #parseaddr return ('name', 'email')
            sender=parseaddr(headers.get('From'))[1],
            recipients=headers.get('To'),
            subject=headers.get('Subject'),
            date=parsedate_to_datetime(headers.get('Date')),
            message=msg_text,
            end_user=get_enduser(headers.get('From')))


@task(name='Read mail using imap protocol')
def read_via_imap(mailbox_id):
    """Fetches mail via imap"""
    #login to imap server
    imap, mailbox = imap_prepare_login(mailbox_id)
    #get dictionary of special folder

    folders = special_folders(imap)
    imap_fetch_inbox(imap, mailbox, folders)
    # We don't need that now
    # imap_fetch_sent(imap, mailbox, folders)
    imap.close()
    imap.logout()


def imap_fetch_inbox(imap, mailbox, folders):
    imap.select(folders['Inbox'])
    status, response = imap.search(None, '(UNSEEN)')
    unread_msg_nums = response[0].split()
    for msg_num in unread_msg_nums:
        _create_msg_interaction(msg_num, imap, mailbox, 'Inbox')


def imap_fetch_sent(imap, mailbox, folders):
    imap.select(folders['Sent'])
    status, response = imap.search(None, "ALL")
    msgs_nums = response[0].split()
    exists_msg_ids = EmailInteraction.objects.filter(
        email_box=mailbox,
        folder='Sent'
    ).values_list('email_id', flat=True)
    for msg_num in msgs_nums:
        _create_msg_interaction(msg_num, imap, mailbox, 'Sent', exists_msg_ids)


def pop3_prepare_login(mailbox_id):
    mailbox_obj = EmailBox.objects.get(id=mailbox_id)
    if mailbox_obj.pop3setting.use_tls:
        pop = poplib.POP3_SSL(
            mailbox_obj.pop3setting.server,
            mailbox_obj.pop3setting.port,
            timeout=60
        )
    else:
        pop = poplib.POP3(
            mailbox_obj.pop3setting.server,
            mailbox_obj.pop3setting.port,
            timeout=60
        )
    try:

        pop.user(mailbox_obj.username)
        pop.pass_(mailbox_obj.get_password())
    except poplib.error_proto as e:
        print("Login failed:", e)
    return pop


def create_emailinteraction_pop3(mailbox_id, msg_text, headers, msg_id):
    EmailInteraction.objects.create(
        folder='Inbox',
        case=get_case_from_subject(headers.get('Subject')),
        email_box_id=mailbox_id,
        email_id=msg_id,
        sender=parseaddr(headers.get('From'))[1],
        recipients=headers.get('To'),
        subject=headers.get('Subject'),
        date=parsedate_to_datetime(headers.get('Date')),
        message=msg_text,
        end_user=get_enduser(headers.get('From')))


def decode_headers(headers):
    """Decode all email headers"""
    decoded_headers = {}
    for key, value in headers:
        value = header.decode_header(value)[0][0]
        try:
            value = isinstance(value, bytes) and value.decode('utf-8') or value
        except UnicodeDecodeError:
            value_info = chardet.detect(value)
            value = value.decode(value_info['encoding'])
        decoded_headers[key] = value
    return decoded_headers


def decode_message_txt(message):
    charset = message.get_content_charset()
    msg_text = ''
    if message.is_multipart():
        while True:
            try:
                payloaded_msg = message.get_payload(0)
            except TypeError:
                msg_text = message.get_payload(decode=True)
                charset = message.get_content_charset()
                break
            message = payloaded_msg
    else:
        #if is_multipart == False get_payload returns byte string
        msg_text = message.get_payload(decode=True)
    if charset:
        msg_text = msg_text.decode(charset)
    else:
        value_info = chardet.detect(msg_text)
        msg_text = msg_text.decode(value_info['encoding'])
    if message.get_content_subtype() == 'html':
        soup = BeautifulSoup(msg_text)
        msg_text = soup.findAll(text=True)
        msg_text = str.join(u'\n', map(str, msg_text))
    return msg_text


@task(name='Read mail using pop3 protocol')
def read_via_pop3(mailbox_id):
    """Fetches only inbox mail via POP3"""
    #login to mailbox

    pop = pop3_prepare_login(mailbox_id)
    messages = [pop.retr(i)[1] for i in range(1, pop.stat()[0] + 1)]
    for index, message in enumerate(messages):
        message = b'\n'.join(message) + b'\n\n'
        message = message_from_bytes(message)
        headers = decode_headers(message.items())
        msg_id = pop.uidl(index+1).split()[2]
        msgs_id_list = EmailInteraction.objects.filter(
            email_box_id=mailbox_id
        ).values_list('email_id', flat=True)
        if msg_id not in msgs_id_list:
            msg_text = decode_message_txt(message)
            create_emailinteraction_pop3(mailbox_id, msg_text, headers, msg_id)
    pop.quit()

